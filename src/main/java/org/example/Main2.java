package org.example;

import org.example.entity.Student;
import org.example.utility.StudentParser;
import org.hibernate.SessionFactory;
import org.example.repository.StudentRepository;
import org.example.utility.HibernateUtil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Main2 {

    public static void main(String[] args) {

        Path filePath = Paths.get("C:\\Users\\AlexandruDescultescu\\IdeaProjects\\PracticalProject\\src\\main\\resources\\students.csv");
        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        final StudentRepository studentRepository = new StudentRepository(sessionFactory.createEntityManager());
        try {
            List<String> lines = Files.readAllLines(filePath);
            for (String line : lines) {
                Student student = StudentParser.fromCSV(line);
                studentRepository.saveOrUpdateStudent(student);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }
}
