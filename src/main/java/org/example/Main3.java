package org.example;

import org.example.entity.Student;
import org.example.repository.StudentRepository;
import org.example.utility.HibernateUtil;
import org.example.utility.StudentParser;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class Main3 {
    public static void main(String[] args) {


        Path filePath = Paths.get("export.csv");
        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        final StudentRepository studentRepository = new StudentRepository(sessionFactory.createEntityManager());

        List<Student> students = studentRepository.findAllStudents();
        for (Student student : students) {
            String s = StudentParser.toCSV(student);
            try {
                Files.writeString(filePath, s, StandardOpenOption.APPEND);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
