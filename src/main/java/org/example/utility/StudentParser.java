package org.example.utility;

import org.example.entity.Student;

public class StudentParser {

    public static Student fromCSV(String line) {
        String[] data = line.split(",");  // aici tot ce se citeste din CSV vor
        // fi toate stringuri... de aceea mai jos se parseaza in tipul de date din obiect
        return new Student(data[0], Boolean.parseBoolean(data[1]),
                Integer.parseInt(data[2]));
    }

    public static String toCSV(Student student){
        StringBuilder sb = new StringBuilder(); // folosit atunci cand se fac multe
        // concatenari fara a se face multe stringuri separate
        sb.append(student.getName());
        sb.append(",");
        sb.append(student.isFullTime());
        sb.append(",");
        sb.append(student.getAge());
        sb.append("\n");
        return sb.toString();
    }
}
