package org.example.utility;

import org.example.entity.Course;
import org.example.entity.Department;
import org.example.entity.Student;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

    public static SessionFactory getSessionFactory() {
        final org.hibernate.SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Department.class)
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();
        return sessionFactory;
    }
}

