package org.example;

import org.example.entity.Course;
import org.example.entity.Department;
import org.example.repository.CourseRepository;
import org.example.repository.DepartmentRepository;
import org.hibernate.SessionFactory;
import org.example.entity.Student;
import org.example.repository.StudentRepository;
import org.example.utility.HibernateUtil;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        SessionFactory sessionFactory007 = HibernateUtil.getSessionFactory();

        StudentRepository studentRepository007 =
                new StudentRepository(sessionFactory007.createEntityManager());

        CourseRepository courseRepository =
                new CourseRepository(sessionFactory007.createEntityManager());

        DepartmentRepository departmentRepository =
                new DepartmentRepository(sessionFactory007.createEntityManager());


        Student student1 = new Student("George", true, 23);
        Student student2 = new Student("Alina", false, 24);
        Student student3 = new Student("Rebecca", true, 19);
        Student student4 = new Student("Ilie", true, 23);
        List<Student> myListOfStudents = Arrays.asList(student1, student2, student3, student4);
        studentRepository007.saveAllStudents(myListOfStudents);

        Department department1 = new Department("Physics");
        departmentRepository.saveOrUpdateDepartment(department1);
//
//
        Course course1 = new Course("Rezistenta materialelor");
        Course course2 = new Course("Planul inclinat");
//        department1.setCourses(Arrays.asList(course1,course2));

        course1.setDepartment(department1);
        course2.setDepartment(department1);
        courseRepository.saveOrUpdateCourse(course1);
//        courseRepository.saveOrUpdateCourse(course2);

//
//        student1.setCourses(Arrays.asList(course1,course2));
//        student1.setAge(55);
//        student1.setFullTime(false);
//
////        studentRepository007.saveOrUpdateStudent(student1);

//        student2.setCourses(Arrays.asList(course2));
//        student2.setAge(33);
//
//        student3.setCourses(Arrays.asList(course1));
//
////        student4.setCourses(Arrays.asList(course1,course2));
//        student4.setAge(56);
//        student4.setFullTime(true);

        List<Student> students008 = studentRepository007.findAllStudents();
        studentRepository007.saveAllStudents(students008);


//        List<Department> departments008 = departmentRepository.findAllDepartments();
//        List<Course> courses008 = courseRepository.findAllCourses();
//
//        studentRepository007.deleteAllStudents(students008);
//       departmentRepository.deleteAllDepartments(departments008);
//       courseRepository.deleteAllCourses(courses008);



    }
}