package org.example.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "course")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Course {
    @Id
    @GeneratedValue
    @Column(name = "course_id")
    private int id;

    @Column(name = "course_name")
    private String name;

    @JoinColumn(name = "course_dept_id",
            referencedColumnName = "dept_id")
    @ManyToOne
    private Department department;

    public Course(String name) {
        this.name = name;
    }
}
