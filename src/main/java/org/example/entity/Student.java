package org.example.entity;

import javax.persistence.*;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "student")
public class Student {
    @Id
    @GeneratedValue
    @Column(name = "student_id")
    private int id;

    @Column(name = "student_name")
    private String name;

    @Column(name = "student_fulltime")
    private boolean fullTime;

    @Column(name = "student_age")
    private int age;

    @ManyToMany
    @JoinTable(name = "enrollment",
            joinColumns = {@JoinColumn(name = "student_id")},
            inverseJoinColumns = {@JoinColumn(name = "course_id")})
    private List<Course> courses;

    public Student(String name, boolean fullTime, int age) {
        this.name = name;
        this.fullTime = fullTime;
        this.age = age;
    }
}
