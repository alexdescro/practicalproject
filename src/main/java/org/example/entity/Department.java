package org.example.entity;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity (name = "department")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Department {
    @Id
    @GeneratedValue
    @Column (name = "dept_id")
    private int id;

    @Column (name = "dept_name")
    private String name;

    @OneToMany (mappedBy = "department")
    private List<Course> courses;

    public Department(String name) {
        this.name = name;
    }
}
