package org.example.repository;

import org.example.entity.Course;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class CourseRepository {

    // mai jos creăm interfața pe post de manager al relatiei cu baza de date
    private EntityManager entityManager;

    public CourseRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    // în continuare vrem să definim toate metodele de lucru cu baza de date
    //              CRUD - Create, Read, Update, Delete

    // metoda CREATE sau UPDATE
    public Course saveOrUpdateCourse(Course course) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            entityManager.persist(course);  // pregateste  obiectul pentru salvare in baza de date
            entityTransaction.commit(); // salveaza obiectul in baza de date
            return course;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // metoda READ_One
    public Course findCourseById(Integer id) {
        return entityManager.find(Course.class, id);
    }

    // metoda READ_All
    public List<Course> findAllCourses() {
        return entityManager.createQuery("FROM course", Course.class).getResultList();
    }


    // metoda DELETE_One
    public Course deleteCourse(Course course) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            entityManager.remove(course);
            entityTransaction.commit();
            return course;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // metoda DELETE_All
    public List<Course> deleteAllCourses(List<Course> courseListToDelete) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            for (Course course : courseListToDelete) {
                entityManager.remove(course); // pregateste obiectul pentru
                // stergerea din baza de date.... similar cu add .. din GIT
            }
            entityTransaction.commit();
            return courseListToDelete;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
