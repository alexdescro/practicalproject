package org.example.repository;

import org.example.entity.Student;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class StudentRepository {

    // mai jos creăm interfața pe post de manager al relatiei cu baza de date
    private EntityManager entityManager;

    public StudentRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    // în continuare vrem să definim toate metodele de lucru cu baza de date
    //              CRUD - Create, Read, Update, Delete

    // metoda CREATE_One sau UPDATE_one
    public Student saveOrUpdateStudent(Student student) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            entityManager.persist(student);  // prepare object to be saved/updated
            entityTransaction.commit();  // perform the action of saving/updating from above
            return student;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // metoda READ_One
    public Student findStudentById(Integer id) {
        return entityManager.find(Student.class, id);
    }

    // metoda READ_All
    public List<Student> findAllStudents() {
        return entityManager.createQuery("FROM student", Student.class).getResultList();
    }


    //    metoda CREATE_All sau UPDATE_All
    public List<Student> saveAllStudents(List<Student> studentList) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            for (Student student : studentList) {
                entityManager.persist(student);  // pregateste obiectul pentru salvare
                // in baza de date.... similar cu add .. din GIT
            }
            entityTransaction.commit(); // salveaza obiectul în baza de date.. similar
            // cu commit din GIT
            return studentList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // metoda DELETE_One
    public Student deleteStudent(Student student) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            entityManager.remove(student);
            entityTransaction.commit();
            return student;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // metoda DELETE_All
    public List<Student> deleteAllStudents(List<Student> listOfStudentsToBeDeleted) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            for (Student student : listOfStudentsToBeDeleted) {
                entityManager.remove(student);  // pregateste obiectul pentru
                // stergerea din baza de date.... similar cu add .. din GIT
            }
            entityTransaction.commit(); // salveaza obiectul în baza de date..
            // similar cu commit din GIT
            return listOfStudentsToBeDeleted;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
