package org.example.repository;

import org.example.entity.Department;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class DepartmentRepository {

    // mai jos creăm interfața pe post de manager al relatiei cu baza de date
    private EntityManager entityManager;

    public DepartmentRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    // în continuare vrem să definim toate metodele de lucru cu baza de date
    //              CRUD - Create, Read, Update, Delete

    // metoda CREATE sau UPDATE
    public Department saveOrUpdateDepartment(Department department) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            entityManager.persist(department);  // pregateste  obiectul pentru salvare in baza de date
            entityTransaction.commit(); // salveaza obiectul in baza de date
            return department;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // metoda READ_One
    public Department findDepartmentById(Integer id) {
        return entityManager.find(Department.class, id);
    }

    // metoda READ_All
    public List<Department> findAllDepartments() {
        return entityManager.createQuery("FROM department", Department.class).getResultList();
    }


    // metoda DELETE_One
    public Department deleteDepartment(Department department) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            entityManager.remove(department);
            entityTransaction.commit();
            return department;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // metoda DELETE_All
    public List<Department> deleteAllDepartments(List<Department> departmentsToBeDeleted) {
        try {
            EntityTransaction entityTransaction = entityManager.getTransaction();
            if (!entityTransaction.isActive()) {
                entityTransaction.begin();
            }
            for (Department department : departmentsToBeDeleted) {
                entityManager.remove(department);  // pregateste obiectul pentru
                // stergerea din baza de date.... similar cu add .. din GIT
            }
            entityTransaction.commit();
            return departmentsToBeDeleted;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
