package org.example;

import org.example.entity.Course;
import org.example.entity.Department;
import org.example.entity.Student;
import org.example.repository.CourseRepository;
import org.example.repository.DepartmentRepository;
import org.example.repository.StudentRepository;
import org.example.utility.HibernateUtil;
import org.hibernate.SessionFactory;

import java.util.List;


public class MainEraseData {
    public static void main(String[] args) {

        SessionFactory sessionFactory009 = HibernateUtil.getSessionFactory();


        StudentRepository studentRepository009 =
                new StudentRepository(sessionFactory009.createEntityManager());

        List<Student> studentsList009 = studentRepository009.findAllStudents();
        studentRepository009.deleteAllStudents(studentsList009);


        CourseRepository courseRepository009 =
                new CourseRepository(sessionFactory009.createEntityManager());

        List<Course> coursesList009 = courseRepository009.findAllCourses();
        courseRepository009.deleteAllCourses(coursesList009);


        DepartmentRepository departmentRepository =
                new DepartmentRepository(sessionFactory009.createEntityManager());

        List<Department> departmentsList009 = departmentRepository.findAllDepartments();
        departmentRepository.deleteAllDepartments(departmentsList009);

    }
}
